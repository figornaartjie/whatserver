from classes.message import Message
from classes.profile import Profile
from classes.auth import RolesAuth

from eve import Eve

if __name__ == '__main__':
	app = Eve(auth=RolesAuth)
	app.on_insert_accounts += RolesAuth.add_token
	app.run(debug=True)

