from eve import Eve
from eve.auth import TokenAuth
from werkzeug.security import check_password_hash

class RolesAuth(TokenAuth):        
        def check_auth(self, token, allowed_roles, resource, method):
                # use Eve's own db driver; no additional connections/resources are used
                accounts = app.data.driver.db['accounts']
                lookup = {'token': token}
                if allowed_roles:
                    # only retrieve a user if his roles match ``allowed_roles``
                    lookup['roles'] = {'$in': allowed_roles}
                account = accounts.find_one(lookup)
                # set 'AUTH_FIELD' value to the account's ObjectId
                # (instead of _Id, you might want to use ID_FIELD)
                self.set_request_auth_value(account['_id'])
                return account and check_password_hash(account['password'], password)
                
        def add_token(documents):
                 for document in documents:
                         document["token"] = (''.join(random.choice(string.ascii_uppercase)
                                 for x in range(12)))          
         
